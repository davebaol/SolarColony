package com.infectedbytes.solarcolony;

import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.infectedbytes.solarcolony.enums.Message;
import com.infectedbytes.solarcolony.statemachines.MoveJob;

/**
 * Created by Henrik on 22.12.2015.
 */
public class Room extends GameEntity implements Json.Serializable {
    public static int globalIndex = 0;
    public Vector2 size;
    private Array<Connector> allConnections = new Array<Connector>();
    private Array<Connection<Room>> establishedConnections = new Array<Connection<Room>>();
    private TextureRegion textureRegion;
    private int index;
    public Array<Quantity> buildCost = new Array<Quantity>();
    public ProducerConsumer producerConsumer;
    public String shortName;
    public String longName;

    @Override
    public String getName() { return shortName; }

    public void updateIndex() { this.index = globalIndex++; }

    public Vector2 getCenter() { return new Vector2(position.x + size.x / 2, position.y + size.y / 2); }

    public boolean hit(int worldX, int worldY) {
        return worldX >= position.x && worldX < position.x + size.x && worldY >= position.y && worldY < position.y + size.y;
    }

    /**
     * This constructor should only be used by json deserialization
     */
    @Deprecated
    @SuppressWarnings("unused")
    public Room() {}

    private Room(GameWorld world, Vector2 size, TextureRegion region) {
        super(world);
        this.size = size;
        this.textureRegion = region;
    }

    public Room(GameWorld world, int u, int v, int w, int h) {
        this(world, new Vector2(w, h), new TextureRegion(Resources.roomSheet, u, v, w, h));
    }

    @Override
    public boolean handleMessage(Telegram msg) {
        if(msg.sender == this) return false;
        if(producerConsumer != null) return producerConsumer.handleMessage(msg);
        return super.handleMessage(msg);
    }

    @Override
    public void addedToWorld(GameWorld world) {
        super.addedToWorld(world);
        Messenger.addListener(this, Message.MoveResources);
    }

    @Override
    public void removedFromWorld(GameWorld world) {
        super.removedFromWorld(world);
        Messenger.removeListener(this, Message.MoveResources);
    }

    public void add(Connector connector) { allConnections.add(connector); }

    public boolean tryConnectTo(Room other) {
        boolean result = false;
        for(Connector ca : getAllConnections()) {
            for(Connector cb : other.getAllConnections()) {
                if(ca.getX() == cb.getX() && ca.getY() == cb.getY()) {
                    establishedConnections.add(ca);
                    other.establishedConnections.add(cb);
                    ca.other = other;
                    cb.other = this;
                    result = true;
                }
            }
        }
        return result;
    }

    public void disconnect() {
        Array<Room> connected = new Array<Room>();
        for(Connection<Room> connection : establishedConnections)
            connected.add(((Connector)connection).other);
        for(Room room : connected) tryDisconnectFrom(room);
        establishedConnections.clear();
    }

    public boolean tryDisconnectFrom(Room other) {
        boolean result = false;
        for(Connector ca : getAllConnections()) {
            for(Connector cb : other.getAllConnections()) {
                if(ca.getX() == cb.getX() && ca.getY() == cb.getY()) {
                    establishedConnections.removeValue(ca, true);
                    other.establishedConnections.removeValue(cb, true);
                    ca.other = null;
                    cb.other = null;
                    result = true;
                }
            }
        }
        return result;
    }

    public Array<Connector> getAllConnections() { return allConnections; }

    public boolean canConnectTo(Connector connector) {
        float nx = connector.getX();
        float ny = connector.getY();
        for(Connector con : allConnections) {
            float mx = position.x + con.localPosition.x;
            float my = position.y + con.localPosition.y;
            if(nx == mx && ny == my) return true;
        }
        return false;
    }

    public void setPosition(float x, float y) { position.set(x, y); }

    public void update(float delta) {
        if(producerConsumer != null)
            producerConsumer.update(delta, world, this);
    }

    public void render(SpriteBatch batch) {
        batch.draw(textureRegion, position.x, position.y);
        if(world != null) { // if world==null then this room is only a prototype
            for(Connector con : allConnections) {
                con.render(batch);
            }
        }
        if(producerConsumer != null) {
            if(producerConsumer.isWaiting())
                batch.draw(Resources.hourglass, position.x + size.x - 18, position.y + size.y - 18);
            if(producerConsumer.offline)
                batch.draw(Resources.offline, position.x + 2, position.y + size.y - 18);
        }
        if(storages.size > 0) {
            float pos = position.x + 2;
            for(Storage s : storages) {
                int sum = s.getSum();
                int limit = s.getLimit();
                if(sum >= limit * 0.95f) {
                    batch.draw(Resources.exclamationMark, pos, position.y + 2);
                    pos += 16;
                }
            }
        }
    }

    public Room simpleCopy() {
        Room room = new Room(world, size, textureRegion);
        room.shortName = shortName;
        room.longName = longName;
        room.position = new Vector2(position);
        for(Connector connector : allConnections) {
            Connector con = new Connector(room, connector.localPosition.x, connector.localPosition.y, connector.direction);
            con.other = connector.other;
            room.allConnections.add(con);
        }
        for(Storage s : this.storages) room.storages.add(new Storage(s));
        for(Quantity q : this.buildCost) room.buildCost.add(new Quantity(q));
        if(this.producerConsumer != null) room.producerConsumer = new ProducerConsumer(this.producerConsumer);
        return room;
    }

    public String getTooltip() {
        StringBuilder sb = new StringBuilder();
        sb.append(longName);
        sb.append("\n");
        appendProductionString(sb);
        sb.append("Cost to build:\n");
        for(Quantity q : buildCost) {
            sb.append(q.resource.shortName);
            sb.append(":");
            sb.append(q.amount);
            sb.append("\n");
        }
        return sb.toString();
    }

    private void appendProductionString(StringBuilder sb) {
        if(producerConsumer == null) return;
        if(producerConsumer.consume.size == 0 && producerConsumer.produce.size == 0) return;

        if(producerConsumer.consume.size > 0 && producerConsumer.produce.size > 0) {
            sb.append("[RED]");
            printArray(sb, producerConsumer.consume);
            sb.append("[]\n->[GREEN]");
            printArray(sb, producerConsumer.produce);
            sb.append("[]");
        } else if(producerConsumer.consume.size > 0) {
            sb.append("[RED]consumes:\n");
            printArray(sb, producerConsumer.consume);
            sb.append("[]");
        } else if(producerConsumer.produce.size > 0) {
            sb.append("[GREEN]produces:\n");
            printArray(sb, producerConsumer.produce);
            sb.append("[]");
        }
        sb.append("\n");
    }

    @Override
    public String getInfo() {
        if(producerConsumer == null) return longName;
        StringBuilder sb = new StringBuilder();
        sb.append(longName);
        sb.append("\n");

        appendProductionString(sb);
        if(producerConsumer.scheduledJobs.size > 0) {
            sb.append("\n");
            for(MoveJob job : producerConsumer.scheduledJobs) {
                if(job.from == this) {
                    sb.append("remove ");
                    sb.append(job.delivery.resource);
                    sb.append("\n");
                } else if(job.to == this) {
                    sb.append("waiting for ");
                    sb.append(job.delivery.resource);
                    sb.append("\n");
                }
            }
        }
        return sb.toString();
    }

    private void printArray(StringBuilder sb, Array<Quantity> array) {
        sb.append(array.get(0).resource.shortName);
        if(array.size > 1) {
            for(int i = 1; i < array.size; i++) {
                sb.append("+");
                sb.append(array.get(i).resource.shortName);
            }
        }
    }

    public int getIndex() { return index; }

    public Array<Connection<Room>> getConnections() { return establishedConnections; }

    @Override
    public void write(Json json) {
        json.writeValue("size", this.size);
        json.writeValue("position", this.position);
        json.writeValue("shortName", this.shortName);
        json.writeValue("longName", this.longName);
        json.writeValue("allConnections", allConnections, Array.class, Connector.class);
        Util.writeTexureRegion(json, "textureRegion", textureRegion);
        json.writeValue("storages", storages, Array.class, Storage.class);
        json.writeValue("buildCost", buildCost, Array.class, Quantity.class);
        if(producerConsumer != null) json.writeValue("producerConsumer", producerConsumer);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void read(Json json, JsonValue jsonData) {
        this.size = json.readValue("size", Vector2.class, jsonData);
        this.position = json.readValue("position", Vector2.class, jsonData);
        this.shortName = jsonData.getString("shortName");
        this.longName = jsonData.getString("longName");
        this.allConnections = json.readValue("allConnections", Array.class, Connector.class, jsonData);
        for(Connector connector : allConnections) connector.owner = this;
        JsonValue region = jsonData.get("textureRegion");
        textureRegion = new TextureRegion(Resources.roomSheet, region.getInt("x"), region.getInt("y"), region.getInt("width"), region.getInt("height"));
        storages = json.readValue("storages", Array.class, Storage.class, jsonData);
        buildCost = json.readValue("buildCost", Array.class, Quantity.class, jsonData);
        if(jsonData.has("producerConsumer"))
            producerConsumer = json.readValue("producerConsumer", ProducerConsumer.class, jsonData);
    }

    @Override
    public void completeRead(GameWorld world) {
        this.world = world;
        for(Connector connector : allConnections) {
            if(connector.otherID > -1) {
                connector.other = world.getRoom(connector.otherID);
                establishedConnections.add(connector);
            }
        }
    }
}

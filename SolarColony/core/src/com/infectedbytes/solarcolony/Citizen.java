package com.infectedbytes.solarcolony;

import com.badlogic.gdx.ai.fsm.State;
import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.StringBuilder;
import com.infectedbytes.solarcolony.enums.Direction;
import com.infectedbytes.solarcolony.enums.Message;
import com.infectedbytes.solarcolony.enums.Resource;
import com.infectedbytes.solarcolony.statemachines.AI;
import com.infectedbytes.solarcolony.statemachines.MoveJob;
import com.infectedbytes.solarcolony.statemachines.StackMachine;

/**
 * Created by Henrik on 26.12.2015.
 */
public class Citizen extends GameEntity implements Json.Serializable {
    private static final float animationFactor = 4f;
    private int sheetID;
    public Direction heading = Direction.SOUTH;
    private CharacterSheet characterSheet;
    private float frameTime;
    public boolean isWalking;
    public StackMachine<Citizen, State<Citizen>> stateMachine = new StackMachine<Citizen, State<Citizen>>(this, AI.Idle);
    private boolean female;
    public String name;
    public MoveJob job;
    public static final int COST = 25;
    public float health = LIMIT;
    public float hunger = LIMIT;
    public float thirst = LIMIT;
    public float breath = LIMIT;
    public static final int LIMIT = 100;
    public static final int THRESHOLD = 20;
    public static float RATE = 0.5f;
    private Quantity tmpQuantity = new Quantity(Resource.Energy);
    public float idletime;

    public boolean isAlive() { return health > 0; }

    public boolean isCritical() { return health < THRESHOLD; }

    public boolean isHungry() { return hunger < THRESHOLD; }

    public boolean isStarving() { return hunger <= 0; }

    public boolean isThirsty() { return thirst < THRESHOLD; }

    public boolean isDyingOfThirst() { return thirst <= 0; }

    public boolean hasLackOfOxygen() { return breath < LIMIT / 2; }

    public boolean isSuffocating() { return breath <= THRESHOLD; }

    public boolean isAbleToWork() {
        return isAlive() && !isCritical() && !isStarving() && !isDyingOfThirst() && !isSuffocating();
    }

    public boolean hasJob() { return job != null; }

    public boolean isFemale() { return female; }

    @Override
    public String getName() { return name; }

    /**
     * This constructor should only be used by json deserialization
     */
    @Deprecated
    @SuppressWarnings("unused")
    public Citizen() {}

    public Citizen(GameWorld world, int sheetID, boolean female) {
        super(world);
        this.sheetID = sheetID;
        this.female = female;
        if(female) this.characterSheet = Resources.characterSheetsFemale[sheetID];
        else this.characterSheet = Resources.characterSheetsMale[sheetID];
        this.name = Resources.randomName(female);
        this.storages.add(new Storage(100));
    }

    @Override
    public void addedToWorld(GameWorld world) {
        Messenger.addListener(this, Message.MoveResources);
        Messenger.addListener(this, Message.RoomRemoved);
    }

    @Override
    public void removedFromWorld(GameWorld world) {
        Messenger.removeListener(this, Message.MoveResources);
        Messenger.removeListener(this, Message.RoomRemoved);
    }

    public void update(float delta) {
        frameTime += delta;
        float rate = RATE * delta;
        hunger = MathUtils.clamp(hunger - rate, 0, LIMIT);
        thirst = MathUtils.clamp(thirst - rate, 0, LIMIT);
        breath = MathUtils.clamp(breath - rate, 0, LIMIT);
        int waste = 0;
        if(hunger < 90) {
            tmpQuantity.resource = Resource.RehydratableFood;
            tmpQuantity.amount = 0;
            storages.get(0).get(tmpQuantity, 10);
            hunger += tmpQuantity.amount;
            waste += tmpQuantity.amount;
        }
        if(thirst < 90) {
            tmpQuantity.resource = Resource.Water;
            tmpQuantity.amount = 0;
            storages.get(0).get(tmpQuantity, 10);
            thirst += tmpQuantity.amount;
            waste += tmpQuantity.amount;
        }
        if(waste > 0) {
            tmpQuantity.resource = Resource.Waste;
            tmpQuantity.amount = waste / 2 + 1;
            getWorld().addGlobalResource(tmpQuantity);
        }
        if(breath < 95) {
            tmpQuantity.resource = Resource.Oxygen;
            tmpQuantity.amount = 0;
            getWorld().getGlobalResource(tmpQuantity, 1);
            breath += tmpQuantity.amount;
            tmpQuantity.resource = Resource.CarbonDioxide;
            getWorld().addGlobalResource(tmpQuantity);
        }
        if(!isHungry() && !isThirsty() && breath > 75) health += rate;
        if(isStarving()) health -= 2 * rate;
        if(isDyingOfThirst()) health -= 2 * rate;
        if(isSuffocating()) health -= 5 * rate;
        health = MathUtils.clamp(health, 0, LIMIT);
        stateMachine.update();
    }

    @Override
    public boolean handleMessage(Telegram msg) {
        if(msg.message == Message.RoomRemoved.ordinal() && job != null) {
            if(job.from == msg.extraInfo || job.to == msg.extraInfo) job = null;
        }
        return stateMachine.handleMessage(msg);
    }

    public void render(SpriteBatch batch) {
        float x = position.x - Util.CHARACTER_WIDTH / 2;
        float y = position.y - Util.CHARACTER_WIDTH / 2;
        int frame = 0;
        if(isWalking) frame = (int)(frameTime * animationFactor);
        batch.draw(characterSheet.getFrame(heading, frame), x, y);
        if(problems() > 1) batch.draw(Resources.exclamationMark, x, y);
        else if(isCritical()) batch.draw(Resources.health, x, y);
        else if(isHungry()) batch.draw(Resources.hunger, x, y);
        else if(isThirsty()) batch.draw(Resources.thirst, x, y);
        else if(hasLackOfOxygen()) batch.draw(Resources.oxygen, x, y);
    }

    private int problems() {
        int i = 0;
        if(isHungry()) i++;
        if(isThirsty()) i++;
        if(isCritical()) i++;
        if(hasLackOfOxygen()) i++;
        return i;
    }

    public boolean hit(int worldX, int worldY) {
        float x = position.x - Util.CHARACTER_WIDTH / 2;
        float y = position.y - Util.CHARACTER_WIDTH / 2;
        return worldX >= x && worldX < x + Util.CHARACTER_WIDTH && worldY >= y && worldY < y + Util.CHARACTER_HEIGHT;
    }

    @Override
    public String getInfo() {
        if(hasJob()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Job:\n");
            if(job.to == this) { // Getting x for him/herself
                sb.append("Getting ");
                sb.append(job.delivery.resource.shortName);
                sb.append("\nfor ");
                sb.append(female ? "herself\n" : "himself\n");
            } else if(job.from != null && job.to != null) { // Moving x from a to b
                sb.append("Moving ");
                sb.append(job.delivery.resource.shortName);
                sb.append("\nfrom ");
                sb.append(job.from.getName());
                sb.append("\nto ");
                sb.append(job.to.getName());
                sb.append("\n");
            } else if(job.from != null) { // Getting rid of x for a
                sb.append("Getting rid of ");
                sb.append(job.delivery.resource.shortName);
                sb.append("\nfor ");
                sb.append(job.from.getName());
                sb.append("\n");
            } else if(job.to != null) { // Moving x to b
                sb.append("Moving ");
                sb.append(job.delivery.resource.shortName);
                sb.append("\nto ");
                sb.append(job.to.getName());
                sb.append("\n");
            } else {
                sb.append("Moving ");
                sb.append(job.delivery.resource.shortName);
                sb.append("\n");
            }

            if(job.delivery.amount > 0) {
                sb.append("(Amount: ");
                sb.append(job.delivery.amount);
                sb.append(")");
            }
            return sb.toString();
        }
        return "";
    }

    @Override
    public void write(Json json) {
        json.writeValue("name", name);
        json.writeValue("female", female);
        json.writeValue("health", health);
        json.writeValue("hunger", hunger);
        json.writeValue("thirst", thirst);
        json.writeValue("breath", breath);
        json.writeValue("sheetID", sheetID);
        json.writeValue("position", position);
        json.writeValue("heading", heading.ordinal());
        json.writeValue("frameTime", frameTime);
        json.writeValue("storages", storages, Array.class, Storage.class);
        if(hasJob()) json.writeValue("job", job);
        json.writeArrayStart("stack");
        for(State<Citizen> state : stateMachine.getStack()) {
            if(state instanceof AI) json.writeValue(((AI)state).ordinal());
        }
        if(stateMachine.getCurrentState() instanceof AI)
            json.writeValue(((AI)stateMachine.getCurrentState()).ordinal());
        json.writeArrayEnd();
    }

    @Override
    @SuppressWarnings("unchecked")
    public void read(Json json, JsonValue jsonData) {
        name = jsonData.getString("name");
        female = jsonData.getBoolean("female");
        health = jsonData.getFloat("health");
        hunger = jsonData.getFloat("hunger");
        thirst = jsonData.getFloat("thirst");
        breath = jsonData.getFloat("breath");
        sheetID = jsonData.getInt("sheetID");
        if(female) characterSheet = Resources.characterSheetsFemale[sheetID];
        else characterSheet = Resources.characterSheetsMale[sheetID];
        position = json.readValue("position", Vector2.class, jsonData);
        heading = Direction.valueOf(jsonData.getInt("heading"));
        frameTime = jsonData.getFloat("frameTime");
        storages = json.readValue("storages", Array.class, Storage.class, jsonData);
        if(jsonData.has("job")) job = json.readValue("job", MoveJob.class, jsonData);
        if(jsonData.has("stack")) {
            stateMachine.setInitialState(null);
            JsonValue stack = jsonData.get("stack");
            for(int i = 0; i < stack.size; i++)
                stateMachine.changeState(AI.valueOf(stack.getInt(i)));
        }
    }

    @Override
    public void completeRead(GameWorld world) {
        this.world = world;
        if(hasJob()) {
            if(job.fromID >= 0) job.from = world.getRoom(job.fromID);
            if(job.toID >= 0) {
                Room target = world.getRoom(job.toID);
                if(target.producerConsumer != null)
                    target.producerConsumer.scheduledJobs.add(job);
                job.to = target;
            } else if(job.from != null) job.to = null;
            else job.to = this;
        }
    }
}

package com.infectedbytes.solarcolony.ui;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.StringBuilder;
import com.infectedbytes.solarcolony.*;
import com.infectedbytes.solarcolony.enums.Resource;
import com.infectedbytes.solarcolony.useractions.InfoAction;

/**
 * Created by Henrik on 10.01.2016.
 */
public class InfoPanel extends Table {
    private GameState gameState;
    private Label title;
    private Label entityInfo;
    private Label name, amount;
    private HealthMonitor healthMonitor;
    private Cell<HealthMonitor> healthCell;
    private CheckBox online;
    private StringBuilder sbName = new StringBuilder();
    private StringBuilder sbAmount = new StringBuilder();

    public InfoPanel(GameState state) {
        super(Resources.skin);
        this.gameState = state;
        setFillParent(true);
        right();
        add(title = new Label(null, Resources.skin)).row();
        healthCell = add(healthMonitor = new HealthMonitor());
        healthCell.row();
        add(entityInfo = new Label(null, Resources.skin)).row();

        Table table = new Table(Resources.skin);
        table.add(name = new Label(null, Resources.skin)).width(120);
        table.add(amount = new Label(null, Resources.skin)).width(80);
        add(table).row();

        add(online = new CheckBox("online", Resources.skin));
        online.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if(gameState.getUserAction() instanceof InfoAction) {
                    InfoAction a = (InfoAction)gameState.getUserAction();
                    Room room = (Room)a.infoObject;
                    room.producerConsumer.offline = !online.isChecked();
                }
            }
        });
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        sbName.delete(0, sbName.length());
        sbAmount.delete(0, sbAmount.length());
        online.setVisible(false);
        healthCell.setActor(null);

        if(gameState.getUserAction() instanceof InfoAction) {
            InfoAction a = (InfoAction)gameState.getUserAction();
            if(a.infoObject != null) {
                entityInfo(a.infoObject);
                return;
            }
        }
        globalInfo(gameState.getWorld());
    }

    private void entityInfo(GameEntity infoObject) {
        StringBuilder info = new StringBuilder(infoObject.getInfo());
        title.setText(infoObject.getName());
        if(infoObject instanceof Room) {
            Room r = (Room)infoObject;
            if(r.producerConsumer != null) {
                Resource resource = r.producerConsumer.checkConsume(infoObject.getWorld(), infoObject.storages);
                if(resource != null) {
                    info.append("\nmissing: ");
                    info.append(resource.shortName);
                }
                resource = r.producerConsumer.checkProduce(infoObject.getWorld(), infoObject.storages);
                if(resource != null) {
                    info.append("\nno storage: ");
                    info.append(resource.shortName);
                }
                online.setVisible(true);
                online.setChecked(!r.producerConsumer.offline);
            }
        } else {
            healthCell.setActor(healthMonitor);
            healthMonitor.update((Citizen)infoObject);
        }
        entityInfo.setText(info.toString());
        if(infoObject.storages.size > 0) {
            int index = 0;
            for(Storage storage : infoObject.storages) {
                sbName.append("Storage ");
                sbName.append(index++);
                sbName.append(":\n");
                sbAmount.append(storage.getSum());
                sbAmount.append("/");
                sbAmount.append(storage.getLimit());
                sbAmount.append("\n");
                for(Quantity q : storage) addAmount(q.resource, q.amount, false);
            }
        }
        name.setText(sbName.toString());
        amount.setText(sbAmount.toString());
    }

    private void globalInfo(GameWorld world) {
        title.setText("Global");
        entityInfo.setText("Resources");
        for(Resource resource : Resource.values())
            addAmount(resource, world.getGlobalAmount(resource), true);
        name.setText(sbName.toString());
        amount.setText(sbAmount.toString());
    }

    private void addAmount(Resource resource, int amount, boolean showEmpty) {
        if(showEmpty || amount > 0) {
            sbName.append(resource.shortName);
            sbName.append(":\n");
            sbAmount.append(amount);
            sbAmount.append("\n");
        }
    }
}

package com.infectedbytes.solarcolony;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.utils.ScreenUtils;

import java.nio.ByteBuffer;

/**
 * Created by Henrik on 10.01.2016.
 */
public class ScreenshotFactory {
    private static final String TAG = "ScreenshotFactory";
    private static int counter = 1;
    private static FileHandle directory = Gdx.files.local("screenshots");

    public static void saveScreenshot() {
        try {
            FileHandle handle;
            do {
                handle = directory.child("screenshot" + (counter++) + ".png");
            } while(handle.exists());
            Pixmap pixmap = takeScreenshot(true);
            PixmapIO.writePNG(handle, pixmap);
            pixmap.dispose();
        } catch(Exception e) {
            Gdx.app.log(TAG, "Exception", e);
        }
    }

    private static Pixmap takeScreenshot(boolean yUp) {
        return takeScreenshot(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), yUp);
    }

    private static Pixmap takeScreenshot(int x, int y, int w, int h, boolean yUp) {
        final Pixmap pixmap = ScreenUtils.getFrameBufferPixmap(x, y, w, h);

        if(yUp) {
            ByteBuffer pixels = pixmap.getPixels();
            int numBytes = w * h * 4;
            byte[] lines = new byte[numBytes];
            int numBytesPerLine = w * 4;
            for(int i = 0; i < h; i++) {
                pixels.position((h - i - 1) * numBytesPerLine);
                pixels.get(lines, i * numBytesPerLine, numBytesPerLine);
            }
            pixels.clear();
            pixels.put(lines);
            pixels.clear();
        }

        return pixmap;
    }
}

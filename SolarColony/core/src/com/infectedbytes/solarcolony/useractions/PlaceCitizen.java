package com.infectedbytes.solarcolony.useractions;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.infectedbytes.solarcolony.Citizen;
import com.infectedbytes.solarcolony.GameState;
import com.infectedbytes.solarcolony.Room;
import com.infectedbytes.solarcolony.Util;

/**
 * Created by Henrik on 26.12.2015.
 */
public class PlaceCitizen extends UserAction {
    private Citizen citizen;

    public PlaceCitizen(GameState state) {
        super(state);
        randomCitizen();
    }

    private void randomCitizen() {
        citizen = new Citizen(getWorld(), Util.nextInt(8), Util.nextBool());
        citizen.breath = 50;
        citizen.hunger = 50;
        citizen.thirst = 50;
    }

    @Override
    public void update(float delta) { }

    @Override
    public void render(SpriteBatch batch) {
        Room room = getWorld().getRoom(citizen.position);
        if(room == null) batch.setColor(Color.RED);
        else if(!gameState.getWorld().canCreateHuman()) batch.setColor(Color.YELLOW);
        else batch.setColor(Color.GREEN);
        citizen.render(batch);
        batch.setColor(Color.WHITE);
    }

    @Override
    public void mouseMoved(int worldX, int worldY) {
        citizen.position.set(worldX, worldY);
    }

    @Override
    public void mouseDown(int worldX, int worldY, int button) {
        super.mouseDown(worldX, worldY, button);
        if(button == 0) {
            Room room = getWorld().getRoom(citizen.position);
            if(room != null && gameState.getWorld().canCreateHuman()) {
                getWorld().createHuman(citizen);
                randomCitizen();
            }
        }
    }
}

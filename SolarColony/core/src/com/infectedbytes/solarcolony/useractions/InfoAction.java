package com.infectedbytes.solarcolony.useractions;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.infectedbytes.solarcolony.*;

/**
 * Created by Henrik on 10.01.2016.
 */
public class InfoAction extends UserAction {
    public GameEntity infoObject;

    public InfoAction(GameState state) { super(state); }

    @Override
    public void update(float delta) {
        super.update(delta);
        if(infoObject instanceof Citizen && !((Citizen)infoObject).isAlive()) infoObject = null;
    }

    @Override
    public void render(SpriteBatch batch) {
        if(infoObject == null) return;
        batch.end();
        ShapeRenderer shapeRenderer = gameState.shapeRenderer;
        shapeRenderer.setColor(Color.WHITE);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        if(infoObject instanceof Room) {
            Room room = (Room)infoObject;
            shapeRenderer.rect(room.position.x, room.position.y, room.size.x, room.size.y);
        } else if(infoObject instanceof Citizen) {
            Citizen citizen = (Citizen)infoObject;
            shapeRenderer.rect(
                    citizen.position.x - Util.CHARACTER_WIDTH / 2, citizen.position.y - Util.CHARACTER_WIDTH / 2,
                    Util.CHARACTER_WIDTH, Util.CHARACTER_HEIGHT);
        }
        shapeRenderer.end();
        batch.begin();
    }

    @Override
    public void mouseDown(int worldX, int worldY, int button) {
        super.mouseDown(worldX, worldY, button);
        if(button == 0) {
            for(Citizen c : gameState.getWorld().citizens) {
                if(c.hit(worldX, worldY)) {
                    infoObject = c;
                    return;
                }
            }
            for(Room room : gameState.getWorld().rooms) {
                if(room.hit(worldX, worldY)) {
                    infoObject = room;
                    return;
                }
            }
            infoObject = null;
        }
    }
}

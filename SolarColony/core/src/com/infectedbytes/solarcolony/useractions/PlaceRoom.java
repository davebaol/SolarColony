package com.infectedbytes.solarcolony.useractions;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.infectedbytes.solarcolony.GameState;
import com.infectedbytes.solarcolony.GameWorld;
import com.infectedbytes.solarcolony.Room;
import com.infectedbytes.solarcolony.Util;

/**
 * Created by Henrik on 26.12.2015.
 */
public class PlaceRoom extends UserAction {
    private Room newRoom;
    private boolean leftDown;

    public PlaceRoom(GameState state, Room room) {
        super(state);
        newRoom = room;
    }

    @Override
    public void render(SpriteBatch batch) {
        if(getWorld().intersects(newRoom)) batch.setColor(Color.RED);
        else if(!getWorld().hasResourcesFor(newRoom)) batch.setColor(Color.YELLOW);
        else batch.setColor(Color.GREEN);
        newRoom.render(batch);
        batch.setColor(Color.WHITE);
    }

    @Override
    public void mouseMoved(int worldX, int worldY) {
        worldX = Util.floorCoord(worldX);
        worldY = Util.floorCoord(worldY);
        newRoom.setPosition(worldX, worldY);
    }

    @Override
    public void mouseDragged(int worldX, int worldY) {
        super.mouseDragged(worldX, worldY);
        worldX = Util.floorCoord(worldX);
        worldY = Util.floorCoord(worldY);
        newRoom.setPosition(worldX, worldY);
        if(leftDown && getWorld().canBuild(newRoom)) {
            getWorld().build(newRoom);
        }
    }

    @Override
    public void mouseDown(int worldX, int worldY, int button) {
        super.mouseDown(worldX, worldY, button);
        if(button == 0) {
            leftDown = true;
            if(getWorld().canBuild(newRoom)) {
                worldX = Util.floorCoord(worldX);
                worldY = Util.floorCoord(worldY);
                newRoom.setPosition(worldX, worldY);
                getWorld().build(newRoom);
            }
        }
    }

    @Override
    public void mouseUp(int worldX, int worldY, int button) {
        super.mouseUp(worldX, worldY, button);
        if(button == 0) leftDown = false;
    }
}
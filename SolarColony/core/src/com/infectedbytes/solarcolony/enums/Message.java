package com.infectedbytes.solarcolony.enums;

/**
 * Created by Henrik on 09.01.2016.
 */
public enum Message {
    RoomAdded, RoomRemoved,
    CitizenAdded, CitizenRemoved,
    MoveResources
}

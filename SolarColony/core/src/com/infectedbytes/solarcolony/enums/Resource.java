package com.infectedbytes.solarcolony.enums;

/**
 * Created by Henrik on 02.01.2016.
 */
public enum Resource { // Super/subscript: ²³⁴⁵⁶⁷⁸⁹₂₃₄₅₆₇₈₉
    // Gas
    Hydrogen("H₂", "Hydrogen", Category.Gas),
    //Deuterium("²H", "Deuterium", Category.Gas),
    //Tritium("³H", "Tritium", Category.Gas),
    //Helium4("⁴He", "Helium 4", Category.Gas),
    Oxygen("O₂", "Oxygen", Category.Gas),
    CarbonDioxide("CO₂", "Carbon Dioxide", Category.Gas),
    Methane("CH₄", "Methane", Category.Gas),

    // Liquid
    Water("H₂O", "Water", Category.Liquid),
    Biomass("Bio", "Biomass", Category.Liquid),
    Waste("Waste", Category.Liquid),

    // Solid
    Rock("Rock", Category.Solid),
    Metal("Metal", Category.Solid),
    Silicon("S", "Silicon", Category.Solid),
    Electronics("Electronics", Category.Solid),
    Plastic("Plastic", Category.Solid),
    DryWaste("Dry Waste", Category.Solid),

    // Food
    RehydratableFood("Food", "Rehydratable Food", Category.Food),

    //Meta
    Energy("Energy", Category.Meta);


    public final String shortName;
    public final String longName;
    public final Category category;
    public final boolean storableInTank;
    public final boolean globalAccess;

    Resource(String shortName, Category category) {
        this(shortName, shortName, category);
    }

    Resource(String shortName, String longName, Category category) {
        this.shortName = shortName;
        this.longName = longName;
        this.category = category;
        this.storableInTank = category == Category.Gas || category == Category.Liquid;
        this.globalAccess = storableInTank || category == Category.Meta;
    }

    public static Resource valueOf(int ordinal) { return values()[ordinal]; }
}

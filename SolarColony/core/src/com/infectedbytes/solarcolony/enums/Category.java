package com.infectedbytes.solarcolony.enums;

/**
 * Created by Henrik on 02.01.2016.
 */
public enum Category {
    // "normal" Resources
    Gas, Liquid, Solid,
    // Extra resources
    Food, Meta;

    public static final int ALL = -1; // all bits one

    public final int bit;
    Category() {
        this.bit = 1 << this.ordinal();
    }

    /**
     * Whether this Category is part of the given flag
     * @param flag
     * @return
     */
    public boolean isInSet(int flag) {
        return (flag & this.bit) == this.bit;
    }

    public static int getFlag(Category... allowed) {
        int result = 0;
        for(Category c : allowed) result |= c.bit;
        return result;
    }

    public static Category valueOf(int ordinal) { return values()[ordinal]; }
}

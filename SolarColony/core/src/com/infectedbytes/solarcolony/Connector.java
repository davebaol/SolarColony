package com.infectedbytes.solarcolony;

import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.infectedbytes.solarcolony.enums.Direction;

/**
 * Created by Henrik on 22.12.2015.
 */
public class Connector implements Connection<Room>, Json.Serializable {
    public Vector2 localPosition;
    public Room owner;
    public Room other;
    public int otherID; // used for deserialization
    public Direction direction;
    private TextureRegion textureRegion;

    /**
     * This constructor should only be used by json deserialization
     */
    @Deprecated
    @SuppressWarnings("unused")
    public Connector() {}

    public Connector(Room owner, float localX, float localY, Direction direction) {
        this.owner = owner;
        this.localPosition = new Vector2(localX, localY);
        this.direction = direction;
        this.textureRegion = Resources.getDirection(direction);
    }

    public float getX() {
        return owner.position.x + localPosition.x;
    }

    public float getY() {
        return owner.position.y + localPosition.y;
    }

    public Vector2 getPosition() { return new Vector2(getX(), getY()); }

    public void render(SpriteBatch batch) {
        if(other != null) return; // rooms is connected -> do not draw this
        int half = Util.CONNECTOR_WIDTH / 2;
        int thickness = (Util.TILE_SIZE - Util.CONNECTOR_WIDTH) / 2;
        switch(direction) {
            case NORTH:
                batch.draw(textureRegion, getX() - half, getY() - thickness);
                break;
            case SOUTH:
                batch.draw(textureRegion, getX() - half, getY());
                break;
            case WEST:
                batch.draw(textureRegion, getX(), getY() - half);
                break;
            case EAST:
                batch.draw(textureRegion, getX() - thickness, getY() - half);
                break;
        }
    }

    @Override
    public float getCost() { return owner.size.len(); }

    @Override
    public Room getFromNode() { return owner; }

    @Override
    public Room getToNode() { return other; }

    @Override
    public void write(Json json) {
        json.writeValue("localPosition", localPosition);
        if(other != null) json.writeValue("other", other.getIndex());
        json.writeValue("direction", direction);
    }

    @Override
    public void read(Json json, JsonValue jsonData) {
        //owner and other will be set by parent room
        localPosition = json.readValue("localPosition", Vector2.class, jsonData);
        if(jsonData.has("other")) otherID = jsonData.getInt("other");
        else otherID = -1;
        direction = json.readValue("direction", Direction.class, jsonData);
        textureRegion = Resources.getDirection(direction);
    }
}

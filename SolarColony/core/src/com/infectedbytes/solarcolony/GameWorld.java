package com.infectedbytes.solarcolony;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.msg.MessageManager;
import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.ai.pfa.indexed.IndexedAStarPathFinder;
import com.badlogic.gdx.ai.pfa.indexed.IndexedGraph;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.infectedbytes.solarcolony.enums.Message;
import com.infectedbytes.solarcolony.enums.Resource;
import com.infectedbytes.solarcolony.enums.StorageType;
import com.infectedbytes.solarcolony.pathfinding.BasicGraphPath;
import com.infectedbytes.solarcolony.pathfinding.EuclideanHeuristic;

/**
 * Created by Henrik on 23.12.2015.
 */
public class GameWorld implements IndexedGraph<Room>, Json.Serializable {
    private final Rectangle tmpRect1 = new Rectangle();
    private final Rectangle tmpRect2 = new Rectangle();
    public Array<Room> rooms = new Array<Room>();
    public Array<Citizen> citizens = new Array<Citizen>();
    private Array<Citizen> removal = new Array<Citizen>();
    private IndexedAStarPathFinder<Room> pathfinder;
    public float totalTime;
    public GameState gameState;
    private Array<Asteroid> asteroids = new Array<Asteroid>();
    private Array<Room> collectors = new Array<Room>();
    public float demolishFactor = 0.5f; // how much resources we get back on destroy

    public GameWorld() {
        Messenger.manager.clear();
        for(int i = 0; i < 200; i++) {
            Asteroid asteroid = Resources.newAsteroid();
            asteroid.position.x = Util.nextFloat() * (Gdx.graphics.getWidth() + 500) - 500;
            asteroids.add(asteroid);
        }
    }

    private void add(Room room) {
        room.setWorld(this);
        // connect rooms
        for(Room other : rooms)
            room.tryConnectTo(other);
        rooms.add(room);
        room.updateIndex();
        pathfinder = new IndexedAStarPathFinder<Room>(this);
        room.addedToWorld(this);
        Messenger.dispatch(Message.RoomAdded, room);
    }

    private void add(Citizen c) {
        citizens.add(c);
        c.addedToWorld(this);
        Messenger.dispatch(Message.CitizenAdded, c);
    }

    public boolean hasCloningFacility() {
        for(Room room : rooms) {
            if(room.shortName.toLowerCase().contains("cloning")) return true; // so incredible ugly code...
        }
        return false;
    }

    public boolean canCreateHuman() {
        if(gameState.godmode) return true;
        if(getGlobalAmount(Resource.Water) < Citizen.COST) return false;
        if(getGlobalAmount(Resource.Oxygen) < Citizen.COST) return false;
        if(getGlobalAmount(Resource.Biomass) < Citizen.COST) return false;
        return true;
    }

    public boolean createHuman(Citizen c) {
        if(!canCreateHuman()) return false;
        if(!gameState.godmode) {
            Quantity q = new Quantity(Resource.Water);
            getGlobalResource(q, Citizen.COST);
            q.resource = Resource.Oxygen;
            getGlobalResource(q, Citizen.COST);
            q.resource = Resource.Biomass;
            getGlobalResource(q, Citizen.COST);
        }
        add(c);
        return true;
    }

    public void remove(Citizen c) {
        citizens.removeValue(c, true);
        c.removedFromWorld(this);
        Messenger.dispatch(Message.CitizenRemoved, c);
    }

    public BasicGraphPath findPath(Room start, Room end) {
        if(pathfinder == null) pathfinder = new IndexedAStarPathFinder<Room>(this);
        BasicGraphPath path = new BasicGraphPath(start, end);
        if(pathfinder.searchConnectionPath(start, end, EuclideanHeuristic.instance, path))
            return path;
        return null;
    }

    public boolean canBuild(Room room) {
        return !intersects(room) && hasResourcesFor(room);
    }

    public boolean intersects(Room newRoom) {
        tmpRect1.setPosition(newRoom.position);
        tmpRect1.setSize(newRoom.size.x, newRoom.size.y);
        for(Room room : rooms) {
            tmpRect2.setPosition(room.position);
            tmpRect2.setSize(room.size.x, room.size.y);
            if(Intersector.overlaps(tmpRect1, tmpRect2)) return true;
        }
        return false;
    }

    public boolean hasResourcesFor(Room room) {
        if(gameState.godmode) return true;
        for(Quantity quantity : room.buildCost) {
            if(getGlobalAmount(quantity.resource) < quantity.amount) return false;
        }
        return true;
    }

    /**
     * builds and adds the room if possible
     *
     * @param proto
     * @return
     */
    public boolean build(Room proto) {
        if(intersects(proto)) return false;
        if(!hasResourcesFor(proto)) return false;
        proto = proto.simpleCopy();
        if(!gameState.godmode) {
            Quantity tmp = new Quantity(Resource.Energy);
            for(Quantity quantity : proto.buildCost) {
                tmp.resource = quantity.resource;
                getGlobalResource(tmp, quantity.amount);
            }
        }
        add(proto);
        return true;
    }

    public boolean canDestroy(Room room) {
        if(room == null) return false;
        for(Citizen c : citizens)
            if(room.hit((int)c.position.x, (int)c.position.y)) return false;
        return true;
    }

    public void destroy(Room room) {
        if(!canDestroy(room)) return;
        room.disconnect();
        rooms.removeValue(room, true);
        Room.globalIndex = 0;
        for(Room r : rooms) r.updateIndex();
        room.removedFromWorld(this);
        room.world = null;
        pathfinder = null;
        Messenger.dispatch(Message.RoomRemoved, room);
    }

    public Room getClosestOutStorage(Resource resource, Vector2 pos) {
        Vector2 tmp = new Vector2();
        Room room = null;
        float min = Float.POSITIVE_INFINITY;
        for(Room r : rooms) {
            boolean hasIt = false;
            for(Storage storage : r.storages) {
                if(storage.getType().isOut)
                    if(storage.getAmount(resource) > 0) {
                        hasIt = true;
                        break;
                    }
            }
            if(hasIt) {
                float len2 = tmp.set(r.getCenter()).sub(pos).len2();
                if(len2 < min) {
                    min = len2;
                    room = r;
                }
            }
        }
        return room;
    }

    public Room getClosestInStorage(Resource resource, Vector2 pos) {
        Vector2 tmp = new Vector2();
        Room room = null;
        float min = Float.POSITIVE_INFINITY;
        for(Room r : rooms) {
            boolean hasIt = false;
            for(Storage storage : r.storages) {
                if(storage.getType() == StorageType.Global)
                    if(storage.isAllowed(resource) && storage.freeSpace() > 0) {
                        hasIt = true;
                        break;
                    }
            }
            if(hasIt) {
                float len2 = tmp.set(r.getCenter()).sub(pos).len2();
                if(len2 < min) {
                    min = len2;
                    room = r;
                }
            }
        }
        return room;
    }


    public Array<Room> getClosestRooms(Room newRoom) {
        Array<Room> result = new Array<Room>();
        int size = Util.TILE_SIZE;
        tmpRect1.setPosition(newRoom.position.x - size, newRoom.position.y - size);
        tmpRect1.setSize(newRoom.size.x + 2 * size, newRoom.size.y + 2 * size);
        for(Room room : rooms) {
            tmpRect2.setPosition(room.position);
            tmpRect2.setSize(room.size.x, room.size.y);
            if(Intersector.overlaps(tmpRect1, tmpRect2))
                result.add(room);
        }
        return result;
    }

    public Room getRoom(int id) { return rooms.get(id); }

    public Room getRoom(Vector2 p) { return getRoom((int)p.x, (int)p.y); }

    public Room getRoom(int worldX, int worldY) {
        for(Room room : rooms) {
            if(worldX >= room.position.x && worldX < room.position.x + room.size.x
                    && worldY >= room.position.y && worldY < room.position.y + room.size.y)
                return room;
        }
        return null;
    }

    /**
     * get amount of items in Global | Output
     *
     * @param resource
     * @return
     */
    public int getGlobalAmount(Resource resource) {
        int amount = 0;
        for(Room room : rooms) {
            for(Storage storage : room.storages) {
                if(storage.getType() == StorageType.Global || storage.getType() == StorageType.Output) {
                    amount += storage.getAmount(resource);
                }
            }
        }
        return amount;
    }

    /**
     * move items from Global | Output to resource
     *
     * @param resource
     * @param amount
     * @return
     */
    public int getGlobalResource(Quantity resource, int amount) {
        int totalAmount = amount;
        for(Room room : rooms) {
            for(Storage storage : room.storages) {
                if(storage.getType() == StorageType.Global || storage.getType() == StorageType.Output) {
                    amount -= storage.get(resource, amount);
                    if(amount == 0) return totalAmount;
                }
            }
        }
        return totalAmount - amount;
    }

    /**
     * adds as much as possible to Global storage
     *
     * @param resource
     * @return
     */
    public int addGlobalResource(Quantity resource) {
        int totalAmount = resource.amount;
        for(Room room : rooms) {
            for(Storage storage : room.storages) {
                if(storage.getType() == StorageType.Global) {
                    storage.add(resource);
                    if(resource.amount == 0) return totalAmount;
                }
            }
        }
        return totalAmount - resource.amount;
    }

    /**
     * Get free space for item in Global storage
     *
     * @param resource
     * @return
     */
    public int getFreeSpace(Resource resource) {
        int amount = 0;
        for(Room room : rooms) {
            for(Storage storage : room.storages) {
                if(storage.getType() == StorageType.Global && storage.isAllowed(resource)) {
                    amount += storage.freeSpace();
                }
            }
        }
        return amount;
    }

    public void update(float delta) {
        if(gameState.isPaused()) return;
        collectors.clear();
        spawnAsteroid();
        totalTime += delta;
        MessageManager.getInstance().update();
        for(Room room : new Array.ArrayIterable<Room>(rooms)) {
            if(room.longName.contains("Collector")) collectors.add(room);
            room.update(delta);
        }
        for(Citizen c : citizens) {
            c.update(delta);
            if(!c.isAlive()) removal.add(c);
        }
        citizens.removeAll(removal, true);
        removal.clear();
        for(int i = asteroids.size - 1; i >= 0; i--) {
            Asteroid asteroid = asteroids.get(i);
            asteroid.update(delta);
            if(asteroid.position.x < -500) asteroids.removeIndex(i);
            else {
                tmpRect1.set(asteroid.position.x, asteroid.position.y, asteroid.width, asteroid.height);
                for(Room room : collectors) {
                    tmpRect2.set(room.position.x, room.position.y, room.size.x, room.size.y);
                    if(Intersector.overlaps(tmpRect1, tmpRect2)) asteroids.removeIndex(i);
                }
            }
        }
    }

    private void spawnAsteroid() {
        if(Util.nextFloat() < 0.1f && asteroids.size < 200) {
            asteroids.add(Resources.newAsteroid());
        }
    }

    public void render(SpriteBatch batch) {
        float cx = gameState.getCamera().position.x;
        float cy = gameState.getCamera().position.y;
        float bx = cx - Gdx.graphics.getWidth() / 2 + (float)(Math.sin(totalTime / 100) * 200 - 200);
        float by = cy - Gdx.graphics.getHeight() / 2 + (float)(Math.cos(totalTime / 100) * 100 - 100);
        batch.draw(Resources.background, bx - cx / 100, by - cy / 100);
        for(Asteroid asteroid : asteroids)
            asteroid.render(batch);
        for(Room room : rooms)
            room.render(batch);
        for(Citizen c : citizens)
            c.render(batch);
    }

    @Override
    public int getNodeCount() { return rooms.size; }

    @Override
    public int getIndex(Room node) { return node.getIndex(); }

    @Override
    public Array<Connection<Room>> getConnections(Room fromNode) { return fromNode.getConnections(); }

    @Override
    public void write(Json json) {
        json.writeValue("time", totalTime);
        json.writeValue("rate", Citizen.RATE);
        json.writeValue("demolishFactor", demolishFactor);
        json.writeValue("rooms", rooms, Array.class, Room.class);
        json.writeValue("citizens", citizens, Array.class, Citizen.class);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void read(Json json, JsonValue jsonData) {
        totalTime = jsonData.getFloat("time");
        Citizen.RATE = jsonData.getFloat("rate");
        demolishFactor = jsonData.getFloat("demolishFactor");
        rooms = json.readValue("rooms", Array.class, Room.class, jsonData);
        Room.globalIndex = 0;
        for(Room room : rooms) {
            room.updateIndex();
            room.completeRead(this);
            room.addedToWorld(this);
        }
        Room.globalIndex = rooms.size;

        citizens = json.readValue("citizens", Array.class, Citizen.class, jsonData);
        for(Citizen citizen : citizens) {
            citizen.completeRead(this);
            citizen.addedToWorld(this);
        }
    }
}

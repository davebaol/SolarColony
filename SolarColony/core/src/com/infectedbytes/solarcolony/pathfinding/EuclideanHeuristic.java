package com.infectedbytes.solarcolony.pathfinding;

import com.badlogic.gdx.ai.pfa.Heuristic;
import com.infectedbytes.solarcolony.Room;

/**
 * Created by Henrik on 26.12.2015.
 */
public class EuclideanHeuristic implements Heuristic<Room> {
    public static final EuclideanHeuristic instance = new EuclideanHeuristic();

    private EuclideanHeuristic() {}

    @Override
    public float estimate(Room node, Room endNode) {
        return endNode.getCenter().sub(node.getCenter()).len();
    }
}

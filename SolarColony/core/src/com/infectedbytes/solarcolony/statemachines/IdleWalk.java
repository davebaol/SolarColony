package com.infectedbytes.solarcolony.statemachines;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.infectedbytes.solarcolony.Citizen;
import com.infectedbytes.solarcolony.Room;
import com.infectedbytes.solarcolony.Util;
import com.infectedbytes.solarcolony.enums.Direction;

import java.util.Random;

/**
 * Created by Henrik on 09.01.2016.
 */
public class IdleWalk extends StateAdapter<Citizen> {
    private static final float IdleSpeed = GotoState.SPEED * 0.75f;
    private Vector2 target;
    private Vector2 tmp = new Vector2();

    @Override
    public void enter(Citizen entity) {
        Room room = entity.getWorld().getRoom(entity.position);
        Vector2 center = room.getCenter();
        float sx = room.size.x / 2;
        float sy = room.size.y / 2;
        float dx = Util.nextFloat() * sx - sx / 2;
        float dy = Util.nextFloat() * sy - sy / 2;
        target = new Vector2(center.x + dx, center.y + dy);
        entity.isWalking = true;
    }

    @Override
    public void exit(Citizen entity) {
        entity.isWalking = false;
    }

    @Override
    public void update(Citizen entity) {
        if(GotoState.near(entity, target)) {
            entity.stateMachine.revertToPreviousState();
        } else {
            float delta = IdleSpeed * Gdx.graphics.getDeltaTime();
            tmp.set(target).sub(entity.position);
            entity.heading = Direction.valueOf(tmp.x, tmp.y);
            if(tmp.len() < delta) entity.position.set(target);
            else entity.position.add(tmp.nor().scl(delta));
        }
    }
}

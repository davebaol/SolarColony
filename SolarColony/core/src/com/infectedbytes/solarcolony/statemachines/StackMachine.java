package com.infectedbytes.solarcolony.statemachines;

import com.badlogic.gdx.ai.fsm.DefaultStateMachine;
import com.badlogic.gdx.ai.fsm.State;
import com.badlogic.gdx.utils.Array;

/**
 * Created by Henrik on 10.01.2016.
 */
public class StackMachine<E, S extends State<E>> extends DefaultStateMachine<E, S> {
    private Array<S> stateStack;

    public StackMachine(E owner, S initialState) { super(owner, initialState, null); }

    public Iterable<S> getStack() { return new Array.ArrayIterable<S>(stateStack); }

    @Override
    public void setInitialState(S state) {
        if(stateStack == null) stateStack = new Array<S>();
        this.stateStack.clear();
        this.currentState = state;
    }

    @Override
    public S getCurrentState() { return currentState; }

    @Override
    public S getPreviousState() {
        if(stateStack.size == 0) return null;
        else return stateStack.peek();
    }

    @Override
    public void changeState(S newState) { changeState(newState, true); }
    public void popAndChangeState(S newState) {
        revertToPreviousState();
        changeState(newState);
    }

    @Override
    public boolean revertToPreviousState() {
        if(stateStack.size == 0) return false;
        S previousState = stateStack.pop();
        changeState(previousState, false);
        return true;
    }

    private void changeState(S newState, boolean pushCurrentStateToStack) {
        if(pushCurrentStateToStack && currentState != null) stateStack.add(currentState);
        if(currentState != null) currentState.exit(owner);
        currentState = newState;
        currentState.enter(owner);
    }
}

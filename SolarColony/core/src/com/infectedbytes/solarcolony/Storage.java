package com.infectedbytes.solarcolony;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.ObjectIntMap;
import com.infectedbytes.solarcolony.enums.Category;
import com.infectedbytes.solarcolony.enums.Resource;
import com.infectedbytes.solarcolony.enums.StorageType;

import java.util.Iterator;

/**
 * Created by Henrik on 02.01.2016.
 */
public class Storage implements Iterable<Quantity>, Json.Serializable {
    private ObjectIntMap<Resource> resources = new ObjectIntMap<Resource>();
    private int limit;
    private int allowedCategories;
    private StorageType type = StorageType.Global;

    public StorageType getType() { return type; }

    public void setType(StorageType type) { this.type = type; }

    /**
     * This constructor should only be used by json deserialization
     */
    @Deprecated
    @SuppressWarnings("unused")
    public Storage() {}

    public Storage(int limit) {
        if(limit < 1) throw new IllegalArgumentException("limit must be at least 1");
        this.limit = limit;
        this.allowedCategories = Category.ALL;
    }

    public Storage(Storage s) {
        for(ObjectIntMap.Entry<Resource> e : s.resources) this.resources.put(e.key, e.value);
        this.limit = s.limit;
        this.allowedCategories = s.allowedCategories;
        this.type = s.type;
    }

    public void setAllowedCategories(Category... allowed) {
        allowedCategories = Category.getFlag(allowed);
    }

    /**
     * adds as much as possible from the given Quantity to this Storage.
     *
     * @param res out value. Holds the rest amount
     * @return the amount of moved items
     */
    public int add(Quantity res) {
        if(!res.resource.category.isInSet(allowedCategories)) return 0; // not allowed in this storage
        int freeSpace = freeSpace();
        if(freeSpace == 0) return 0; // storage is full => nothing to do here!
        int amount = res.amount < freeSpace ? res.amount : freeSpace;
        res.amount -= amount;
        resources.getAndIncrement(res.resource, 0, amount);
        return amount;
    }

    /**
     * removes a resource from this storage. The supplied amount is moved from this storage to the given Quantity.
     *
     * @param res    The target Quantity
     * @param amount How many units of the given Resource should be moved?
     * @return the amount of moved items
     */
    public int get(Quantity res, int amount) {
        int stored = getAmount(res.resource);
        if(amount > stored) {
            res.amount += stored;
            resources.getAndIncrement(res.resource, 0, -stored);
            return stored;
        } else {
            res.amount += amount;
            resources.getAndIncrement(res.resource, 0, -amount);
            return amount;
        }
    }

    public int getSum() {
        int sum = 0;
        for(ObjectIntMap.Entry<Resource> e : resources) sum += e.value;
        return sum;
    }

    public int freeSpace() {
        return limit - getSum();
    }

    public int getAmount(Resource res) { return resources.get(res, 0); }

    public int getLimit() { return limit; }

    public boolean isAllowed(Category category) { return category.isInSet(allowedCategories); }

    public boolean isAllowed(Resource resource) { return isAllowed(resource.category); }

    /**
     * Returns a new iterator. Remove is unsupported. Note that always the same Quantity instance is returned!
     *
     * @return
     */
    @Override
    public Iterator<Quantity> iterator() {
        return new StorageIterator();
    }

    @Override
    public void write(Json json) {
        json.writeValue("limit", limit);
        json.writeValue("allowedCategories", allowedCategories);
        json.writeValue("type", type.ordinal());
        json.writeArrayStart("keys");
        for(ObjectIntMap.Entry<Resource> entry : this.resources)
            if(entry.value > 0) json.writeValue(entry.key.ordinal());
        json.writeArrayEnd();
        json.writeArrayStart("values");
        for(ObjectIntMap.Entry<Resource> entry : this.resources)
            if(entry.value > 0) json.writeValue(entry.value);
        json.writeArrayEnd();
    }

    @Override
    public void read(Json json, JsonValue jsonData) {
        limit = jsonData.getInt("limit");
        allowedCategories = jsonData.getInt("allowedCategories");
        type = StorageType.valueOf(jsonData.getInt("type"));
        int[] keys = jsonData.get("keys").asIntArray();
        int[] values = jsonData.get("values").asIntArray();
        for(int i = 0; i < keys.length; i++)
            resources.put(Resource.valueOf(keys[i]), values[i]);
    }

    /**
     * Note that the same Quantity instance is returned!
     */
    private class StorageIterator implements Iterator<Quantity> {
        private Iterator<ObjectIntMap.Entry<Resource>> iterator;
        private Quantity entry = new Quantity(Resource.Energy);

        public StorageIterator() { iterator = resources.iterator(); }

        @Override
        public boolean hasNext() { return iterator.hasNext(); }

        @Override
        public Quantity next() {
            ObjectIntMap.Entry<Resource> e = iterator.next();
            entry.resource = e.key;
            entry.amount = e.value;
            return entry;
        }

        @Override
        public void remove() { throw new UnsupportedOperationException(); }
    }
}

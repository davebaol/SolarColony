package com.infectedbytes.solarcolony;

import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.ai.msg.Telegraph;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

/**
 * Created by Henrik on 02.01.2016.
 */
public abstract class GameEntity implements Telegraph {
    public Array<Storage> storages = new Array<Storage>();
    protected GameWorld world;
    public Vector2 position = new Vector2();

    public boolean invalid() { return world == null; }

    public abstract String getName();

    public GameWorld getWorld() { return world; }

    public void setWorld(GameWorld world) { this.world = world; }

    /**
     * This constructor should only be used by json deserialization
     */
    @Deprecated
    public GameEntity() {}

    public GameEntity(GameWorld world) { this.world = world; }

    @Override
    public boolean handleMessage(Telegram msg) { return false; }

    public void addedToWorld(GameWorld world) { this.world = world; }
    public void removedFromWorld(GameWorld world) {}

    public void completeRead(GameWorld world) {}

    public String getInfo() { return ""; }
}
